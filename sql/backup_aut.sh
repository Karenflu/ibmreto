#!/bin/bash

USER="root"
PASSWORD=""
DATABASE="reto"

FINAL_OUTPUT=reto/`date +%Y%m%d`_$DATABASE.sql
mysqldump --user=$USER --password=$PASSWORD $DATABASE > $FINAL_OUTPUT
gzip $FINAL_OUTPUT