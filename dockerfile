FROM node:latest
WORKDIR /src
COPY package*.json ./
RUN npm install -g nodemon
RUN npm install
COPY . .
CMD ["node", "app.js"]