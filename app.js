const http = require('http');
const express = require('express');
const app = express();
const server = http.createServer(app);
const dbConnection = require('./dbConnection.js');
const log4js = require('log4js');


  log4js.configure({
    appenders: {
      out: { type: 'console' },
      app: { type: 'file', filename: 'logs/aplicacion.log' }
    },
    categories: {
      default: { appenders: [ 'out', 'app' ], level: 'debug' }
    }
  });
const connection = dbConnection();
var logger = log4js.getLogger();
logger.level = 'debug';
logger.debug("INICIO- MENSAJES DEBUG");

app.get('/',(req,res)=>{
res.send('Bienvenido');
});

app.get('/reto',(req,res)=>{
    res.json([{'Response': 'Enviar 2 parametros'}]);
    logger.info("parametros incompletos");    
});
app.get('/reto/:num1/',(req,res)=>{
    res.json([{'Response': 'Enviar 2do parametro'}]);
    logger.info("parametros incompletos");
});
app.get('/reto///',(req,res)=>{
    res.json([{'Response': 'Enviar 2 parametros'}]);
    logger.info("parametros incompletos");    
});
app.get('/reto//:num2',(req,res)=>{
    res.json([{'Response': 'Enviar 1er Parametros'}]); 
    logger.info("parametros incompletos");       
});

app.get('/reto/:num1/:num2',(req,res)=>{
    
    let response='';
    let numA=0;
    let numB=0;
    var query="";

    if (/^([0-9])*$/.test(req.params.num1)) {
        numA = parseInt(req.params.num1);
      }
    else{
        response=response+'Primer parametro no es un numero';
        logger.warn(response);
    }
    if(req.params.num2!=''){
        if (/^([0-9])*$/.test(req.params.num2)) {
            numB = parseInt(req.params.num2);
        }
        else{
            response=response+' - 2do parametro no es un numero';
            logger.warn(response);
        }
    }else{
        response=response+' - 2do para es nulo';
        logger.warn(response);
    }
    if(response==''){
        logger.debug("NUM 1:"+numA);
        logger.debug("NUM 2:"+numB);
       
        response = numA+numB;
        logger.debug("SUMA:" +response);
        query='INSERT INTO ejercicio (valor1,valor2,resultado) VALUES('+numA+','+numB+','+response+')';
        //response=query;
        connection.query(query,(err, result) => {
            console.log(err+" "+result);
        });
    }

  

    res.json([{'Response': response}]);        
});

server.listen(3000, ()=>{
    console.log('servidor encendido port:'+3000);
});
